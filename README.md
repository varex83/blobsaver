# Blob Saver

### About

This microservice designed to Creating, Getting, Saving and Deleting **Blobs**

**Blob** - **User-defined JSON**

### Documentation

You can run OpenAPI Docs with 
```sh
    cd docs
    npm install
    npm run start  
```

### Running microservice

To run blobsaver service with Docker:
```sh
  TODO    
```
or directly:
```sh
    go run main.go run service
```
or 
```sh
    go build -o blobsaver 
    ./blobsaver run service
```
But, firsly you should run PostgreSQL DB on URI you provided in ```config.yaml```
To migrate DB run:
```sh
    go run main.go migrate up
```
or
```sh
    go build -o blobsaver 
    ./blobsaver migrate up
```
