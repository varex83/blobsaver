/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type BlobAttributes struct {
	// Blob JSON
	Body  json.RawMessage `json:"body"`
	Owner string          `json:"owner"`
}
