/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

// Error Object
type NotFound struct {
	// Description of Error
	Detail *string `json:"detail,omitempty"`
	// Status Code of Error
	Status *int32 `json:"status,omitempty"`
	// Title of Error
	Title *string `json:"title,omitempty"`
}
