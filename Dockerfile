FROM golang:1.16

WORKDIR /home/varex/go/go1.17.1/src/gitlab.com/varex83/blobsaver

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /blobsaver gitlab.com/varex83/blobsaver


###

FROM alpine:3.9

COPY --from=0 /home/varex/go/go1.17.1/src/gitlab.com/varex83/blobsaver /home/varex/go/go1.17.1/src/gitlab.com/varex83/blobsaver
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["blobsaver"]
