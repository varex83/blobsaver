package service

import (
	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/varex83/blobsaver/internal/config"
	"gitlab.com/varex83/blobsaver/internal/service/handlers"
	"net"
	"net/http"
)

func Run(cfg config.Config) {
	handlers.ConfigureDB(cfg)
	if err := NewService(cfg).run(); err != nil {
		panic(err)
	}
}
type Service struct {
	log      *logan.Entry
	copus    types.Copus
	listener net.Listener
}

func NewService(cfg config.Config) *Service {
	return &Service{
		log:      cfg.Log(),
		copus:    cfg.Copus(),
		listener: cfg.Listener(),
	}
}

func (s *Service) run() error {
	s.log.Info("Service Started!")
	r := s.router()
	if err := s.copus.RegisterChi(r); err != nil {
		return errors.Wrap(err, "cop failed")
	}
	return http.Serve(s.listener, r)
	return nil
}