package handlers

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/blobsaver/resources"
	"net/http"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	var _blob resources.BlobResponse
	err := json.NewDecoder(r.Body).Decode(&_blob)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	b, err := converterRD(_blob.Data)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	err = bchain.SendData(b)
	if err != nil {
		logrus.Error(err)
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	resp, err := db.Insert(b)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	blob := converterDR(resp)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
	w.WriteHeader(201)
	ape.Render(w, resources.BlobResponse{Data: blob})
}
