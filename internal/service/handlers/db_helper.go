package handlers

import (
	"gitlab.com/varex83/blobsaver/internal/config"
	"gitlab.com/varex83/blobsaver/internal/data"
	"gitlab.com/varex83/blobsaver/internal/data/ledger"
	"gitlab.com/varex83/blobsaver/internal/data/pg"
	"gitlab.com/varex83/blobsaver/resources"
	"strconv"
)

var db data.BlobsQ
var bchain *ledger.Postman

func getLinks(p int, pagesLimit int, maxCount int) *resources.Links {
	first := 1
	last := (maxCount + pagesLimit - 1) / pagesLimit
	prev := p - 1
	if prev < 1 {
		prev = 1
	}
	if prev > last {
		prev = last
	}
	next := p + 1
	if last < next {
		next = last
	}
	if next < first {
		next = first
	}
	return &resources.Links{
		Last:  strconv.Itoa(last),
		First: strconv.Itoa(first),
		Next:  strconv.Itoa(next),
		Prev:  strconv.Itoa(prev),
		Self:  strconv.Itoa(p),
	}
}

func ConfigureDB(cfg config.Config) {
	db = pg.NewBlobsQ(cfg.DB())
	bchain = ledger.NewPostman(cfg)
}

// resources.Blob -> data.Blob
func converterRD(blob resources.Blob) (data.Blob, error) {
	id, err := strconv.Atoi(blob.ID)
	if err != nil {
		return data.Blob{}, err
	}
	return data.Blob{
		ID:    id,
		Owner: blob.Attributes.Owner,
		Body:  blob.Attributes.Body,
	}, nil
}

// data.Blob -> resources.Blob
func converterDR(blob data.Blob) resources.Blob {
	return resources.Blob{
		Key: resources.Key{
			ID:           strconv.Itoa(blob.ID),
			ResourceType: "blob",
		},
		Attributes: &resources.BlobAttributes{
			Body:  blob.Body,
			Owner: blob.Owner,
		},
	}
}
