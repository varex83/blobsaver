package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/blobsaver/internal/service/requests"
	"gitlab.com/varex83/blobsaver/resources"
	"net/http"
)

func GetBlob(w http.ResponseWriter, r *http.Request) {
	id, err := requests.GetID(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	db.FilterById(id)
	defer db.All()
	blob, err := db.Get()
	if err != nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}
	msg := converterDR(blob)
	w.WriteHeader(200)
	ape.Render(w, resources.BlobResponse{Data: msg})
}
