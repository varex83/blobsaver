package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/blobsaver/internal/service/requests"
	"net/http"
)

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	id, err := requests.GetID(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	db.FilterById(id)
	defer db.All()
	err = db.Delete()
	if err != nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}
	w.WriteHeader(204)
}
