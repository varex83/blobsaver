package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/blobsaver/internal/service/requests"
	"gitlab.com/varex83/blobsaver/resources"
	"net/http"
)

const defaultPageLimit = 5


func getPageLimits(p int, pagesLimit int) (int, int) {
	return (p - 1) * pagesLimit, p * pagesLimit
}


func normalize(l *int, r *int, n int) {
	if *r > n {
		*r = n
	}
	if *l < 0 {
		*l = 0
	}
	if *l > *r {
		*l = *r
	}
}

func GetBlobs(w http.ResponseWriter, r *http.Request) {
	page, err := requests.GetPage(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)[0])
		return
	}
	pageLimit, err := requests.GetPageLimit(r)
	if err != nil {
		pageLimit = defaultPageLimit
	}
	owner := r.FormValue("owner")
	var blobs []resources.Blob
	if owner != "" {
		db.FilterByOwner(owner)
		defer db.All()
	}
	resp, err := db.Select()
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
	blobs = make([]resources.Blob, len(resp))
	for i, v := range resp {
		blobs[i] = converterDR(v)
	}
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
	leftElement, rightElement := getPageLimits(page, pageLimit)
	normalize(&leftElement, &rightElement, len(blobs))
	w.WriteHeader(200)
	ape.Render(w, resources.BlobListResponse{Data: blobs[leftElement:rightElement],
			Links: getLinks(page, pageLimit, len(blobs))})
}
