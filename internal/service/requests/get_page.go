package requests

import (
	"net/http"
	"strconv"
)

func GetPage(r *http.Request) (int, error) {
	strPage := r.FormValue("page")
	return strconv.Atoi(strPage)
}