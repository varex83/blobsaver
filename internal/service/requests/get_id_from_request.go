package requests

import (
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

func GetID(r *http.Request) (int, error) {
	strId := chi.URLParam(r, "id")
	return strconv.Atoi(strId)
}

