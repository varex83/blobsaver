package requests

import (
	"net/http"
	"strconv"
)

func GetPageLimit(r *http.Request) (int, error) {
	strPage := r.FormValue("limit")
	return strconv.Atoi(strPage)
}