package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/varex83/blobsaver/internal/service/handlers"
	"net/http"
)

const (
	requestPrefix = "/api/v1"
)

func (s *Service) router() chi.Router {

	r := chi.NewRouter()

	r.Use(
		ape.ContentType("application/vnd.api+json"),
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
		),
	)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Header().Set("Content-Type", "text/html")
		w.Write([]byte("Hello world!"))
	})
	r.Route(requestPrefix + "/blobs", func(r chi.Router) {
		r.Get("/{id}", handlers.GetBlob)
		r.Delete("/{id}", handlers.DeleteBlob)
		r.Post("/", handlers.CreateBlob)
		r.Get("/", handlers.GetBlobs)
	})

	return r
}
