package data

import (
	"encoding/json"
)

type Blob struct {
	ID    int             `db:"id" structs:"-"`
	Owner string          `db:"owner" structs:"owner"`
	Body  json.RawMessage `db:"body" structs:"body"`
}

type blob struct {
	ID    int             `json:"id"`
	Owner string          `json:"owner"`
	Body  json.RawMessage `json:"body"`
}

func (b Blob) MarshalJSON() ([]byte, error) {
	bl := &blob{
		b.ID, b.Owner, b.Body,
	}
	resp, err := json.Marshal(bl)
	return []byte(resp), err
}


type BlobsQ interface {
	Get() (Blob, error)
	Select() ([]Blob, error)
	Update() ([]Blob, error)
	Delete() error
	Insert(value Blob) (Blob, error)

	FilterById(id int)
	FilterByOwner(owner string)

	All()
}
