package ledger

import (
	"context"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
	"gitlab.com/varex83/blobsaver/internal/config"
	"gitlab.com/varex83/blobsaver/internal/config/ledgerer"
	"gitlab.com/varex83/blobsaver/internal/data"
	"net/url"
)

const (
	passPhrase	= "TokenD Developer Network"
	txExpire	= 601200
)

type Postman struct {
	TxBuilder		*xdrbuild.Builder
	Tx				*xdrbuild.Transaction
	Horizon 		*horizon.Connector
	MasterSeed		string
	MasterKey		string
	config 			*ledgerer.LedgerConfig
}

func NewPostman(config config.Config) *Postman {
	cfg, err := config.GetLedger()
	if err != nil {
		logrus.Fatal(errors.Wrap(err, "Error while loading ledger cfg"))
		return nil
	}
	uri, err := url.Parse(cfg.URL)
	if err != nil {
		logrus.Error(errors.Wrap(err, "Postman creation failed:"))
		return nil
	}
	logrus.Info(cfg.Key)
	horiz := horizon.NewConnector(uri)
	return &Postman{
		TxBuilder: xdrbuild.NewBuilder(passPhrase, txExpire),
		Tx:      nil,
		config: cfg,
		Horizon: horiz,
		MasterSeed: cfg.Seed,
		MasterKey: cfg.Key,
	}
}

func init() {
	
}

func (pman *Postman) generateTx(blob data.Blob) error {
	pman.Tx = pman.TxBuilder.Transaction(keypair.MustParseAddress(pman.MasterKey))
	pman.Tx.Op(xdrbuild.CreateData{
		Type:  0,
		Value: blob,
	})
	kpMaster, err := keypair.ParseSeed(pman.MasterSeed)
	if err != nil {
		return err
	}
	pman.Tx.Sign(kpMaster)
	return nil
}

func (pman *Postman) SendData(blob data.Blob) error {
	err := pman.generateTx(blob)
	if err != nil {
		return err
	}
	envelope, err := pman.Tx.Marshal()
	if err != nil {
		return err
}
	ctx := context.Background()
	resp := pman.Horizon.Submitter().Submit(ctx, envelope)
	logrus.Info(string(resp.RawResponse))
	if err != nil {
		return err
	}
	return nil
}









