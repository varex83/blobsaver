package data

import (
	"testing"
)

func TestBlob(t *testing.T) {
	b := Blob{
		ID:    0,
		Owner: "Varex",
		Body: []byte("{\"a\":\"b\"}"),
	}
	resp, err := b.MarshalJSON()
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(string(resp))
}
