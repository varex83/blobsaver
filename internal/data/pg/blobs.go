package pg

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/varex83/blobsaver/internal/data"
)

const tableName = "blobs"

type blobsQ struct {
	data.BlobsQ
	db        *pgdb.DB
	sql       sq.SelectBuilder
	sqlUpdate sq.UpdateBuilder
	sqlDelete sq.DeleteBuilder
}

func NewBlobsQ(db *pgdb.DB) data.BlobsQ {
	return &blobsQ{
		db:        db,
		sql:       sq.Select("*").From(tableName),
		sqlUpdate: sq.Update(tableName).Suffix("returning *"),
		sqlDelete: sq.Delete(tableName).Suffix("returning *").From(tableName),
	}
}

func (q *blobsQ) Get() (data.Blob, error) {
	var result data.Blob
	err := q.db.Get(&result, q.sql)
	if err == sql.ErrNoRows {
		return data.Blob{}, nil
	}
	return result, err
}

func (q *blobsQ) Select() ([]data.Blob, error) {
	var result []data.Blob
	err := q.db.Select(&result, q.sql)
	return result, err
}

func (q *blobsQ) Delete() error {
	err := q.db.Exec(q.sqlDelete)
	return err
}

func (q *blobsQ) Insert(value data.Blob) (data.Blob, error) {
	clauses := structs.Map(value)
	clauses["owner"] = value.Owner
	clauses["body"] = value.Body

	var result data.Blob
	stmt := sq.Insert(tableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)

	return result, err
}

func (q *blobsQ) FilterById(id int) {
	stmt := sq.Eq{"id": id}
	q.sql = q.sql.Where(stmt)
	q.sqlUpdate = q.sqlUpdate.Where(stmt)
	q.sqlDelete = q.sqlDelete.Where(stmt)
}

func (q *blobsQ) FilterByOwner(owner string) {
	stmt := sq.Eq{"owner": owner}
	q.sql = q.sql.Where(stmt)
	q.sqlUpdate = q.sqlUpdate.Where(stmt)
	q.sqlDelete = q.sqlDelete.Where(stmt)
}

func (q *blobsQ) All() {
	q.sql = sq.Select("*").From(tableName)
	q.sqlUpdate = sq.Update(tableName).Suffix("returning *")
	q.sqlDelete = sq.Delete(tableName).Suffix("returning *").From(tableName)
}
