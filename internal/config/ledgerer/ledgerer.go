package ledgerer

import (
	"fmt"
	"gitlab.com/distributed_lab/kit/kv"
)

type ILedger interface {
	GetLedger() (*LedgerConfig, error)
}

type Ledger struct {
	getter kv.Getter
}

type LedgerConfig struct {
	Seed 		string 	`fig:"seed"`
	Key			string 	`fig:"key"`
	URL 		string 	`fig:"url"`
}


func (l Ledger) GetLedger() (*LedgerConfig, error) {
	config := l.getter
	mp := kv.MustGetStringMap(config, "horizon")
	cfg := &LedgerConfig{
		Key: fmt.Sprintf("%v", mp["key"]),
		Seed: fmt.Sprintf("%v", mp["seed"]),
		URL: fmt.Sprintf("%v", mp["url"]),
	}
	return cfg, nil
}

func NewLedger(kv kv.Getter) Ledger {
	return Ledger{getter: kv}
}
