-- +migrate Up
create table blobs
(
    id    bigserial primary key not null,
    body            jsonb,
    owner           text
);

-- +migrate Down
drop table blobs;
